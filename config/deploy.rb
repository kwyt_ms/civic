# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

set :application, "civic"
set :repo_url, "https://bitbucket.org/kwyt_ms/civic"

set :rbenv_ruby, File.read('.ruby-version').strip
set :branch, ENV['BRANCH'] || "master"

append :linked_files, "config/master.key"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "node_modules"

set :puma_init_active_record, true
set :keep_releases, 2